# We are live!

I've had this site sitting for a while. It's finally to the point that I'm
ready to flip the switch and put some of my writing out there for public consumption.

This is, as you may have inferred from the URL, a Gitlab Pages blog. In order
to use Gitlab Pages, you need to use CI/CD. This is my first real
experience with CI/CD. The only other times I've played with CI/CD, it was
someone else's open source project, where everything was already set up.'
It can be intimidating. I'd look
at the scripts they used and I was glad I didn't have to put together something
like that for my own projects.

It's always fun to learn new things. It turns out that for the basics, 
there's not much to it. There's just one problem. **The automated site
building that was working a few weeks ago doesn't work now.** It throws 
error messages saying Pandoc can't
find files. Given that it used to work (as evidenced by the initial
CI/CD jobs that passed before) I have no idea where to even start to look. There
doesn't seem to be any easy way to debug things to figure out what might
be the problem.

So, for now, I've reverted to a CI/CD script that does nothing. I build
locally, push to the repo, and with a short delay the changes are visible
on the blog.

This also gave me the opportunity to deepen my understanding of Bash. 
The Docker container I'm using is intentionally lightweight. It's
kind of cool to automate everything using a shell script. One downside 
to that is that "Bash" isn't a language. In spite of the fact that
it's installed everywhere, **the version matters a lot**, so you have many
incompatible languages all going by the name "Bash".

That's how these things go. Working through these hiccups is a fun way to get my mind off the
things going on in the world right now. Ultimately, I hope it'll lead me
to post some useful content, which is the whole reason I created a blog.
All the learning is a side effect, not the goal.
