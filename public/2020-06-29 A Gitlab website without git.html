<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <title>2020-06-29 A Gitlab website without git</title>
  <style type="text/css">code{white-space: pre;}</style>
  <script src="/usr/share/javascript/mathjax/MathJax.js?config=TeX-AMS_CHTML-full" type="text/javascript"></script>

<style type="text/css">
body {
    margin: auto;
    max-width: 800px;
    font-size: 130%;
    margin-top: 0px;
    margin-bottom: 90px;
    font-family: Roboto, Arial, sans-serif;
    line-height: 1.6;
}

a {
	text-decoration: none;
}

h1 {
	font-size: 160%;
	line-height: 1.22em;
  font-family: Ubuntu, "Trebuchet MS", sans-serif;
}

h2 {
  font-family: Ubuntu, "Trebuchet MS", sans-serif;
  font-size: 110%;
	line-height: 0.5em;
	margin-bottom: 0px;
}

code {
  font-family: "Ubuntu Mono", "Courier New", monospace;
  font-size: 110%;
}

pre > code {
  padding: .2rem .5rem;
  margin: 0 .2rem;
  white-space: nowrap;
  /*background: #FBFBFB;*/
  border: 1px solid gray;
  border-radius: 1px;
  display: block;
  padding: 1rem 1.5rem;
  white-space: pre; 
}

.header {
  text-align: right;
  color: brown;
  border-bottom: 1px solid silver;
  padding-top: 6px;
  padding-bottom: 6px;
  margin-bottom: 32px;
}

blockquote {
  font-family: Georgia, serif;
}

blockquote:before {
  border-left: 1px solid gray;
}
</style>

</head>
<body>
<div class="header"><a href="/awebsite">My Blog &#9878;</a></div>
<div class="content">
<h1 id="how-to-create-a-gitlab-website-without-knowing-anything-about-git">How to create a Gitlab website without knowing anything about Git</h1>
<p><a href="https://gitlab.com/">Github competitor Gitlab</a> offers a free service called Gitlab Pages that you can use to host a static website. Basically, if you want to host a bunch of text and images, you should be using a static website. It’s simple, secure, and fast. If you want to discuss your new working paper, explain something you’ve learned to others, or tell the world about yourself, static websites are ideal.</p>
<p>If you’re a professional web developer, setting up a static site generator and pushing the result to a Git repo is straightforward. Of course, most of us are not professional web developers, and in that case the process can be intimidating. An awesome feature of Gitlab is that you can use it to create and maintain a static website/blog <em>without knowing anything about Git</em>. You can build a complete static website from the comfort of your browser - even on an iPad or Chromebook.</p>
<p>This post describes the procedure I used to create this blog. I was sitting around one hot summer Sunday afternoon in the days of coronavirus and I wondered how far I could go without leaving the browser on my Chromebook. Here were the constraints of my challenge:</p>
<ul>
<li>Have to do everything from the default browser on a Chromebook.</li>
<li>Others have to be able to use the same setup without doing anything more than copying my configuration files.</li>
<li>Automatic index page generation, with the posts listed in reverse chronological order.</li>
<li>No complicated static site generator configuration required.</li>
<li>Acceptable styling out of the box (not necessarily a design masterpiece, but readable).</li>
<li>Easy to customize for those that want to do so.</li>
</ul>
<p>Just to be clear, I don’t claim I’ve figured out all of this stuff. I’m not a web developer and I only spent two hours on it over the course of one afternoon.</p>
<h2 id="create-a-new-project">Create a new project</h2>
<p>Log into your Gitlab account. Create a new project. You can call it whatever you want, but for the sake of this post, let’s assume you called it “blogo”. Further, we’ll assume your username is “duck”.</p>
<h2 id="add-the-build-script">Add the build script</h2>
<p>Here’s the build script I use to build this website. Click the button inside your repo and create a new file called <code>build</code> (with no extension). Open it in the Web IDE and copy/paste this text into it:</p>
<pre><code>#!/bin/bash
SAVEIFS=$IFS
IFS=$(echo -en &quot;\n\b&quot;)
shopt -s nullglob
rm -f index.md
rm -R public/
mkdir -p public
for f in *.md
do
    echo &quot;Building file $f&quot;
    pandoc -s &quot;$f&quot; -o &quot;public/${f%.*}.html&quot; --template=template.html --mathjax
done
echo &quot;# Post Index\n\n&quot; &gt; index.md
for f in `ls *.md | sort -r`
do
    [[ &quot;$f&quot; = index.md ]] &amp;&amp; continue
    tmp=${f%.*}
    index=${tmp%% *}
    title=${tmp#* }
    echo &quot;- [$title &amp;#9900; ${index:0:10}]($tmp.html)&quot; &gt;&gt; index.md
done
pandoc -s index.md -o public/index.html --template=indextemplate.html
IFS=$SAVEIFS</code></pre>
<p>That might look like gibberish to you. The reason I use a Bash script is because I’m using a minimalist Alpine Docker container to do the building. Bash is the only thing I have available.</p>
<p>What does the script do? First it reads in the names of all markdown files and converts them one by one to html using a template titled <code>template.html</code> that you’ll add to the repo later. Then it creates an index file in reverse chronological order. I like to automate the creation of the index page to keep the overhead at zero, but if you want to maintain your own index, all you have to do is delete the second <code>for...done</code> loop and manually update an <code>index.md</code> file when you add a new post. If you want to create a static website rather than a blog, the concept of automatically generating an index page doesn’t make any sense, so you’ll need to manually update the index page as needed.</p>
<p>A few things to note:</p>
<ul>
<li>You can change the CSS in the template files that you’ll add later. The default CSS here is readable, but not particularly stylish.</li>
<li>The files you create need to have names of this form: <code>2020-06-20 A blog post.md</code>. Only markdown files are converted. Posts are sorted based on an assumption that the date, in that format, comes first. In the index, the date will be moved after the title. Capitalize inside the filename the way you’d prefer the post title to show up in the index.</li>
<li>If you have multiple posts on the same day, do something like <code>2020-06-20_01 A blog post.md</code> and <code>2020-06-20_02 A blog post.md</code> to denote the ordering you want. Only the first 10 characters show up in the index, so the <code>_01</code> and <code>_02</code> won’t show up in the index. Their only use is for sorting.</li>
<li>Technically, you don’t need the date to appear first. The index generator assumes everything up to the first space is the index, and it puts up to the first ten characters of the index next to the title in the index. Of course, it’s an easy change if you want to do something else.</li>
<li>You don’t need to use markdown format. If you want to use, say, .org for your posts, you can change <code>.md</code> in the script to <code>.org</code> everywhere. You can use any of the many input formats supported by Pandoc. You’d have to change the <code>build</code> script to use org format rather than markdown.</li>
</ul>
<h2 id="math-support">Math support</h2>
<p>The build script above includes MathJax support so you can insert equations and other math in your posts. If you really don’t want that, you can delete the MathJax support, but it doesn’t have a meaningful impact on performance if you don’t have any equations.</p>
<p>The notation is standard Pandoc notation. For example, this is how you’d add displayed and inline equations to a post:</p>
<pre><code>This is a displayed equation:
$$y_{t} = \alpha + \beta x_{t} + \varepsilon_{t}$$

And this is an inline equation: $\nu_{t} = \xi_{t} + \phi_{t}$.</code></pre>
<p>It displays like this:</p>
<p>This is a displayed equation: <span class="math display">\[y_{t} = \alpha + \beta x_{t} + \varepsilon_{t}\]</span></p>
<p>And this is an inline equation: <span class="math inline">\(\nu_{t} = \xi_{t} + \phi_{t}\)</span>.</p>
<h2 id="add-the-template-files">Add the template files</h2>
<p>You can copy these template files to your repo, using the same names (<code>template.html</code> and <code>indextemplate.html</code>). Create new files and open them in the Web IDE.</p>
<ul>
<li><a href="https://gitlab.com/bachmeil/examplesite/-/blob/master/template.html">template.html</a></li>
<li><a href="https://gitlab.com/bachmeil/examplesite/-/blob/master/indextemplate.html">indextemplate.html</a></li>
</ul>
<h2 id="add-the-config">Add the config</h2>
<p>The final step is to add your config file. Create a new file titled <code>.gitlab-ci.yml</code> and open it in the Web IDE. Add these lines to it:</p>
<pre><code>image: pandoc/core

pages:
  stage: build
  script:
  - sh build
  stage: deploy
  artifacts:
    paths:
    - public</code></pre>
<h2 id="add-content">Add content</h2>
<p>Create a markdown file and open it in the Web IDE. Be sure to use the naming scheme <code>2020-06-20_A_blog_post.md</code>.</p>
<h2 id="commit">Commit</h2>
<p>Click the blue Commit… button to commit all these files to your repo. Be careful to commit to the master branch rather than creating a new branch (sometimes Gitlab highlights the second option for some reason.)</p>
<h2 id="build">Build</h2>
<p>Your site will automatically build once you make the commit to master. If it builds successfully - and it should if you’ve followed the directions above exactly - you can go open <code>https://duck.gitlab.io/blogo</code> to view your blog index.</p>
<h2 id="make-your-site-visible-to-the-world">Make your site visible to the world</h2>
<p>If you chose to make your project blogo private, you’ll need to allow others to view the website. In the menu on the left, click on <code>&gt; Settings &gt; General &gt; Visibility, project features, permissions</code>. Hit the button to expand that section and set “Pages” to “Everyone”.</p>

<br>
<a href="index.html">&#10554; Back to Index</a>
</div>
<br><br><br>
</body>
</html>
