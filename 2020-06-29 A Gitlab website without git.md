# How to create a Gitlab website without knowing anything about Git

[Github competitor Gitlab](https://gitlab.com/) offers a free service called Gitlab Pages that you can use to host a static website. Basically, if you want to host a bunch of text and images, you should be using a static website. It's simple, secure,  and fast. If you want to discuss your new working paper, explain something you've learned to others, or tell the world about yourself, static websites are ideal.

If you're a professional web developer, setting up a static site generator and pushing the result to a Git repo is straightforward. Of course, most of us are not professional web developers, and in that case the process can be intimidating. An awesome feature of Gitlab is that you can use it to create and maintain a static website/blog *without knowing anything about Git*. You can build a complete static website from the comfort of your browser - even on an iPad or Chromebook.

This post describes the procedure I used to create this blog. I was sitting around one hot summer Sunday afternoon in the days of coronavirus and I wondered how far I could go without leaving the browser on my Chromebook. Here were the constraints of my challenge:

- Have to do everything from the default browser on a Chromebook.
- Others have to be able to use the same setup without doing anything more than copying my configuration files.
- Automatic index page generation, with the posts listed in reverse chronological order.
- No complicated static site generator configuration required.
- Acceptable styling out of the box (not necessarily a design masterpiece, but readable).
- Easy to customize for those that want to do so.

Just to be clear, I don't claim I've figured out all of this stuff. I'm not a web developer and I only spent two hours on it over the course of one afternoon.

## Create a new project

Log into your Gitlab account. Create a new project. You can call it whatever you want, but for the sake of this post, let's assume you called it "blogo". Further, we'll assume your username is "duck".

## Add the build script

Here's the build script I use to build this website. Click the button inside your repo and create a new file called `build` (with no extension). Open it in the Web IDE and copy/paste this text into it:

```
#!/bin/bash
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
shopt -s nullglob
rm -f index.md
rm -R public/
mkdir -p public
for f in *.md
do
	echo "Building file $f"
    pandoc -s "$f" -o "public/${f%.*}.html" --template=template.html --mathjax
done
echo "# Post Index\n\n" > index.md
for f in `ls *.md | sort -r`
do
    [[ "$f" = index.md ]] && continue
    tmp=${f%.*}
    index=${tmp%% *}
    title=${tmp#* }
    echo "- [$title &#9900; ${index:0:10}]($tmp.html)" >> index.md
done
pandoc -s index.md -o public/index.html --template=indextemplate.html
IFS=$SAVEIFS
```

That might look like gibberish to you. The reason I use a Bash script is because I'm using a minimalist Alpine Docker container to do the building. Bash is the only thing I have available.

What does the script do? First it reads in the names of all markdown files and converts them one by one to html using a template titled `template.html` that you'll add to the repo later. Then it creates an index file in reverse chronological order. I like to automate the creation of the index page to keep the overhead at zero, but if you want to maintain your own index, all you have to do is delete the second `for...done` loop and manually update an `index.md` file when you add a new post. If you want to create a static website rather than a blog, the concept of automatically generating an index page doesn't make any sense, so you'll need to manually update the index page as needed.

A few things to note:

- You can change the CSS in the template files that you'll add later. The default CSS here is readable, but not particularly stylish.
- The files you create need to have names of this form: `2020-06-20 A blog post.md`. Only markdown files are converted. Posts are sorted based on an assumption that the date, in that format, comes first. In the index, the date will be moved after the title. Capitalize inside the filename the way you'd prefer the post title to show up in the index.
- If you have multiple posts on the same day, do something like `2020-06-20_01 A blog post.md` and `2020-06-20_02 A blog post.md` to denote the ordering you want. Only the first 10 characters show up in the index, so the `_01` and `_02` won't show up in the index. Their only use is for sorting.
- Technically, you don't need the date to appear first. The index generator assumes everything up to the first space is the index, and it puts up to the first ten characters of the index next to the title in the index. Of course, it's an easy change if you want to do something else.
- You don't need to use markdown format. If you want to use, say, .org for your posts, you can change `.md` in the script to `.org` everywhere. You can use any of the many input formats supported by Pandoc. You'd have to change the `build` script to use org format rather than markdown.

## Math support

The build script above includes MathJax support so you can insert equations and other math in your posts. If you really don't want that, you can delete the MathJax support, but it doesn't have a meaningful impact on performance if you don't have any equations.

The notation is standard Pandoc notation. For example, this is how you'd add displayed and inline equations to a post:

```
This is a displayed equation:
$$y_{t} = \alpha + \beta x_{t} + \varepsilon_{t}$$

And this is an inline equation: $\nu_{t} = \xi_{t} + \phi_{t}$.
```

It displays like this:

This is a displayed equation:
$$y_{t} = \alpha + \beta x_{t} + \varepsilon_{t}$$

And this is an inline equation: $\nu_{t} = \xi_{t} + \phi_{t}$.

## Add the template files

You can copy these template files to your repo, using the same names (`template.html` and `indextemplate.html`). Create new files and open them in the Web IDE.

- [template.html](https://gitlab.com/bachmeil/examplesite/-/blob/master/template.html)
- [indextemplate.html](https://gitlab.com/bachmeil/examplesite/-/blob/master/indextemplate.html)

## Add the config

The final step is to add your config file. Create a new file titled `.gitlab-ci.yml` and open it in the Web IDE. Add these lines to it:

```
image: pandoc/core

pages:
  stage: build
  script:
  - sh build
  stage: deploy
  artifacts:
    paths:
    - public
```

## Add content

Create a markdown file and open it in the Web IDE. Be sure to use the naming scheme `2020-06-20_A_blog_post.md`.

## Commit

Click the blue Commit... button to commit all these files to your repo. Be careful to commit to the master branch rather than creating a new branch (sometimes Gitlab highlights the second option for some reason.)

## Build

Your site will automatically build once you make the commit to master. If it builds successfully - and it should if you've followed the directions above exactly - you can go open `https://duck.gitlab.io/blogo` to view your blog index.

## Make your site visible to the world

If you chose to make your project blogo private, you'll need to allow others to view the website. In the menu on the left, click on `> Settings > General > Visibility, project features, permissions`. Hit the button to expand that section and set "Pages" to "Everyone".
