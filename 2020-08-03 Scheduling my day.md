# Scheduling my day

I've learned that I need to schedule my day when I'm working from home.
I'm not entirely sure why, but on the days that I've tried to work from my home office without a schedule, 
I spent all my time hopelessly trying to figure out what to do rather than doing any actual work.

Some bits and pieces on how I do my scheduling:

- My day is broken into the four blocks
	- Early morning
	- Late morning
	- Early afternoon
	- Late afternoon
- Everything other than the trivial goes on the schedule if I'm going to do it. I schedule my shower every day.
It's not that I'd forget to shower without having it on my schedule. It provides a reminder that I have one 
commitment every day in that block of time that I might otherwise not think about when scheduling other tasks.
- I add tasks to one of the time blocks and 
hope for the best. Estimating the time to complete most tasks is useless. I have no
idea how much time most tasks will take.
- Following David Allen's advice, I don't assign priorities. You've either made a commitment to complete a
task or you haven't. Your commitment to any task falls into one of these categories:
	- I've decided to do the task.
	- I haven't decided to do the task.
    
    There's no such thing as "70% committed, 30% not committed to doing a task". If you haven't committed to 
doing a task, it doesn't get scheduled. I will occasionally schedule time to make a decision on a task if
the decision requires a lot of analysis.
- I don't schedule based on importance. Once I've committed to doing something, I have to do it. Leaving 
it to nag me day after day, but then saying "it's not important enough to put on today's schedule", does not make 
me productive. I schedule one or more minor, not-very-important tasks every day.
Knowing I can do 10-15 of these minor tasks per week means I don't think about them. They're
in the queue. I'll get to them soon enough.
- I keep a list of the few best things to add to my schedule in case I complete everything in one of the time blocks.
This happens with some frequency since I do my best to not overschedule.
- I think about my active projects every day as I make my schedule. This review really helps me to
feel things are not out of control.
- I underschedule. That lets me be realistic about what I can actually get done. It makes me think about the
opportunity cost of committing to do another task. It helps me avoid the trap of agreeing to do things
because it would have value to someone.
- My task management app is Todoist. I have a project with the current date as the name. The new sections feature lets
me break up the day into blocks.
- Scheduling approaches that others have recommended that have not worked for me:
    - Scheduling my time in weeks. Too big a block. Too many things come up during the week.
    - Scheduling the whole day as one block. I overscheduled because I was unable to realistically know how much time things would take.
    - Assign a time for each task. Since I had no idea how much time to assign, this fell apart quickly.
    
    The four blocks per day approach has worked because I find that scheduling three or four tasks in one block is my optimum.
