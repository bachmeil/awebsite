# A common ancestor?

Lundgqvist and Sargent (2012, page 3) write

> All of macroeconomics too seems to have descended from a common source, Irving Fisher's and Milton Friedman's consumption Euler equation

That's a strange claim, in my opinion, on multiple levels. First, they omit any mention of Modigliani, whose work on the topic was deemed worthy of a Nobel Prize. Second, they omit any mention of Keynes, who obviously had a major influence on the development of macroeoconomics. Lest you think they're referring only to *modern* macroeconomics but they simply forgot to add that qualifier, they included Fisher in their statement - someone many years older than Keynes and whose work came before the General Theory. The omission of both Keynes and Modigliani is unlikely to have been an accident - Modigliani was an unapologetic Keynesian, and the Minnesota school has long attempted to write Keynes out of history. Third, it's difficult to reconcile this definition of macroeconomics with the structural VAR literature, the Taylor rule literature, work in central banks, and other works. You would really have to stretch to make a connection between, say, a SVAR analysis of the effects of monetary policy and the PIH.

It seems that a more appropriate to say the authors only read a slice of the macroeconomics literature, and they only consider "pure" academic macroeconomic research to be "macroeconomics". The authors clearly do not consider empirical macroeconomic research or macroeoconomic forecasting to be part of macroeconomics. It's unfortunate that such an excellent textbook used to introduce many graduate students to the field of macroeconomics would engage in such propaganda.
